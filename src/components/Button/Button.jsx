import styles from './Button.module.scss'
import PropTypes from 'prop-types'

function Button({ backgroundColor, text, onClick }){
    return <button
    className = {styles.btn}
    style = {{backgroundColor: backgroundColor, borderColor: backgroundColor}}
    onClick={onClick}
    >{text}</button>
}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Button;